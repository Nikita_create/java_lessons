Задачи в отдельных ветках!!!

1) Create class Person with fields firstName(String), lastName(String), age(int), gender(String), phoneNumber(int),
and  five override methods that set this fields with different arguments list.
Create class that will be use instance of class Person and his methods.

2) Создать интерфейс Calculatable с методом calculateSquare и default метод print, который печатает название фигуры.
Создать абстрактный класс Figure с полями(длина стороны, высота), который реализует интерфейс Calculatable.
Создать классы Triangle, Parallelogram, Romb. В классах наследниках переопределить метод calculateSquare,
для расчета площади. Протестировать функционал.

3) Please create an abstarct class Shape and two subclasses Square and Circle. Replace code in method drawShape()
   according to principles of polymorphism. Please propose the other solutions for improving quality of the code.

   enum ShareType {
       SQUARE, CIRCLE
   }

   class Shape {
       private ShareType shareType;

       //other fields


       public Shape(ShareType shareType) {
           this.shareType = shareType;
       }

       public ShareType getShareType() {
           return shareType;
       }

       public void drawSquare() {
           //Code for drawing square
       }

       public void drawCircle() {
           //Code for drawing square
       }

       public void drawShape() {
           if (getShareType() == ShareType.SQUARE) {
               drawSquare();
           } else {
               drawCircle();
           }
       }

       //others methods
   }

   4) Vehicles task
   5) Tests inside archive