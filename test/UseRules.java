import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class UseRules {

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Test
    public void shouldPrintInConsole() {
        new Calculator().print();

        String log = outRule.getLog();

        Assert.assertTrue(log.contains("Hello"));
    }

}
