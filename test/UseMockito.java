import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;

@RunWith(MockitoJUnitRunner.class)
public class UseMockito {

    @Mock
    private Calculator mock;

    @Spy
    private Calculator spy;

    @Test
    public void shouldMockMethod() {
        Calculator calculator = new Calculator();

//        Calculator mock = Mockito.mock(Calculator.class);

        Mockito.when(mock.getRandomValue()).thenReturn(8);

        Mockito.when(mock.sumRandom(ArgumentMatchers.anyInt())).thenCallRealMethod();

        Assert.assertEquals(14, mock.sumRandom(6));

    }

    @Test
    public void shouldSpyMethod() {

//        Calculator spy = Mockito.spy(Calculator.class);

        Mockito.when(spy.getRandomValue()).thenReturn(8);

        Assert.assertEquals(14, spy.sumRandom(6));

        Mockito.verify(spy, Mockito.times(1)).sumRandom(ArgumentMatchers.anyInt());
        Mockito.verify(spy, Mockito.times(1)).getRandomValue();
        Mockito.verifyNoMoreInteractions(spy);

    }

    @Test
    public void shouldReturnRandomSum() {


        Mockito.when(spy.getRandomValue()).thenReturn(10);

        Assert.assertEquals(15, spy.randomSum());
    }

    @Test
    public void shouldReturnRandomMinus() {
        Mockito.when(spy.getRandomValue()).thenReturn(10);

        Assert.assertEquals(5, spy.randomMinus());
    }

    @Test
    public void shouldReturnDivision() {
        Mockito.when(spy.getRandomValue()).thenReturn(10);

        Assert.assertEquals(2, spy.randomDivision());
    }

    @Test
    public void shouldReturnMultiplication() {
        Mockito.when(spy.getRandomValue()).thenReturn(10);

        Assert.assertEquals(50, spy.randomMultiplication());
    }

    @Test
    public void shouldReturnDivisionWithRemainder() {
        Mockito.when(spy.getRandomValue()).thenReturn(10);

        Assert.assertEquals(0, spy.randomDivisionWithRemainder());
    }
}
