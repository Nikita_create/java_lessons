import lessons.for_test.Car;
import lessons.for_test.Wheel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CarTest {

    @Mock
    private Wheel wheel;

    @InjectMocks
    private Car car;

    @Test
    public void shouldInjectMock() {
        Mockito.when(wheel.size()).thenReturn(12);

        int size = car.getWheelSize();

        Assert.assertEquals(12, size);
    }

}
