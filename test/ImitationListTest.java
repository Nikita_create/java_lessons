import homeworks.imitation_list.ImitationList;
import org.junit.*;

public class ImitationListTest {

    private ImitationList list = new ImitationList(5);

    @Before
    public void clearArray() {
        list.setList(new int[2]);
    }

    @Test
    public void shouldAddElementToArray() {
        list.addElementToList(3);

        int[] array = list.getList();

        Assert.assertEquals(3, array[0]);
    }

    @Test
    public void shouldChangeElementByIndexCorrectIndexAndValue() {
        list.addElementToList(9);

        list.changeElementByIndex(0, 6);

        int[] array = list.getList();

        Assert.assertEquals(6, array[0]);
    }

    @Test
    public void shouldChangeElementByIndexIsNotCorrectValueCorrect() {

        list.changeElementByIndex(8, 6);

        int[] array = list.getList();

        Assert.assertEquals(0, array[0]);
        Assert.assertEquals(0, array[1]);
    }

    @Test
    public void shouldChangeElementByIndexCorrectValueIsNotCorrect() {

        list.changeElementByIndex(1, 0);

        int[] array = list.getList();

        Assert.assertEquals(0, array[0]);
        Assert.assertEquals(0, array[1]);
    }

    @Test
    public void shouldDeleteDuplicate() {
        list.addElementToList(2);
        list.addElementToList(2);
        list.addElementToList(3);

        list.deleteDuplicates();

        int[] array = list.getList();

        Assert.assertEquals(2, array[0]);
        Assert.assertEquals(3, array[1]);
    }

    @Test
    public void shouldSortArray() {
        list.addElementToList(2);
        list.addElementToList(4);
        list.addElementToList(3);
        list.addElementToList(1);

        list.bubbleSort();

        int[] array = list.getList();
/*
        Assert.assertEquals(1,array[0]);
        Assert.assertEquals(2,array[1]);
        Assert.assertEquals(3,array[2]);
        Assert.assertEquals(4,array[3]);*/

//        Assert.assertArrayEquals(array, new int[]{1, 2, 3, 4});

        for (int i = 0; i < array.length; i++) {
            Assert.assertEquals(array[i], i + 1);
        }
    }

    @Test
    public void shouldResizeArray() {
        list.addElementToList(2);
        list.addElementToList(3);
        list.addElementToList(4);

        int[] array = list.getList();

        Assert.assertArrayEquals(array, new int[]{2, 3, 4, 0});
    }
}
