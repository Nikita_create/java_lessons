import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class UseJUnitParams {

    Calculator calculator = new Calculator();

    @Test
    @Parameters({"5|7", "-9|-6", "-7|2"})
    public void shouldReturnSumValues(int value1, int value2) {

        int sum = calculator.sum(value1, value2);

        Assert.assertEquals(sum, value1 + value2);

    }
}
