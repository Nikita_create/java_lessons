import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;



@RunWith(PowerMockRunner.class)
@PrepareForTest(Calculator.class)
public class UsePowermockito {
    @Mock
    private Calculator calcSpy;

    @Test
    public void shouldMockStaticMethod() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomStatic()).thenReturn(10);

        Assert.assertEquals(10, Calculator.getRandomStatic());
    }

    @Test
    public void shouldReturnStaticSum() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomStatic()).thenReturn(10);

        Mockito.when(Calculator.getRandomStaticSecond()).thenReturn(5);

        Mockito.when(calcSpy.randomStaticSum()).thenCallRealMethod();

        Assert.assertEquals(15, calcSpy.randomStaticSum());
    }

    @Test
    public void returnStaticMinus() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomStatic()).thenReturn(10);

        Mockito.when(Calculator.getRandomStaticSecond()).thenReturn(5);

        Mockito.when(calcSpy.randomStaticMinus()).thenCallRealMethod();

        Assert.assertEquals(5, calcSpy.randomStaticMinus());
    }

    @Test
    public void returnStaticMultiplication() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomStatic()).thenReturn(10);

        Mockito.when(Calculator.getRandomStaticSecond()).thenReturn(5);

        Mockito.when(calcSpy.randomStaticMultiplication()).thenCallRealMethod();

        Assert.assertEquals(50, calcSpy.randomStaticMultiplication());
    }

    @Test
    public void returnStaticDivision() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomStatic()).thenReturn(10);

        Mockito.when(Calculator.getRandomStaticSecond()).thenReturn(5);


        Mockito.when(calcSpy.randomStaticDivision()).thenCallRealMethod();

        Assert.assertEquals(2, calcSpy.randomStaticDivision());
    }

    @Test
    public void returnStaticDivisionWithRemainder() {
        PowerMockito.mockStatic(Calculator.class);

        Mockito.when(Calculator.getRandomStatic()).thenReturn(10);

        Mockito.when(Calculator.getRandomStaticSecond()).thenReturn(5);

        Mockito.when(calcSpy.randomStaticDivisionWithRemainder()).thenCallRealMethod();

        Assert.assertEquals(0, calcSpy.randomStaticDivisionWithRemainder());
    }

    @Test
    public void shouldMockPrivateMethod() throws Exception {

        Calculator calculator = new Calculator();

        Calculator spy = PowerMockito.spy(calculator);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getRandomPrivate"))
                .withNoArguments()
                .thenReturn(15);

        Assert.assertEquals(25, spy.sumRandomPrivate(10));

        PowerMockito.verifyPrivate(spy, Mockito.times(1)).invoke("getRandomPrivate");
    }
    @Test
    public void shouldReturnSum() throws Exception {

        Calculator calculator = new Calculator();

        Calculator spy = PowerMockito.spy(calculator);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getFirstRandomPrivate"))
                .withNoArguments()
                .thenReturn(10);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getSecondRandomPrivate"))
                .withNoArguments()
                .thenReturn(5);

        Assert.assertEquals(15, spy.randomPrivateSum());
    }
    @Test
    public void shouldReturnMinus() throws Exception {

        Calculator calculator = new Calculator();

        Calculator spy = PowerMockito.spy(calculator);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getFirstRandomPrivate"))
                .withNoArguments()
                .thenReturn(10);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getSecondRandomPrivate"))
                .withNoArguments()
                .thenReturn(5);

        Assert.assertEquals(5, spy.randomPrivateMinus());
    }
    @Test
    public void shouldReturnMultiplication() throws Exception {

        Calculator calculator = new Calculator();

        Calculator spy = PowerMockito.spy(calculator);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getFirstRandomPrivate"))
                .withNoArguments()
                .thenReturn(10);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getSecondRandomPrivate"))
                .withNoArguments()
                .thenReturn(5);

        Assert.assertEquals(50, spy.randomPrivateMultiplication());
    }
    @Test
    public void shouldReturnDivision() throws Exception {

        Calculator calculator = new Calculator();

        Calculator spy = PowerMockito.spy(calculator);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getFirstRandomPrivate"))
                .withNoArguments()
                .thenReturn(10);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getSecondRandomPrivate"))
                .withNoArguments()
                .thenReturn(5);

        Assert.assertEquals(2, spy.randomPrivateDivision());
    }
    @Test
    public void shouldReturnDivisionWithRemainder() throws Exception {

        Calculator calculator = new Calculator();

        Calculator spy = PowerMockito.spy(calculator);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getFirstRandomPrivate"))
                .withNoArguments()
                .thenReturn(10);

        PowerMockito.when(spy, MemberMatcher.method(Calculator.class, "getSecondRandomPrivate"))
                .withNoArguments()
                .thenReturn(5);

        Assert.assertEquals(0, spy.randomPrivateDivisionWithRemainder());
    }

}
