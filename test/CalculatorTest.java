import lessons.for_test.Calculator;
import org.junit.*;

public class CalculatorTest {

    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("Before all tests");
    }

    @AfterClass
    public static void afterAllTests() {
        System.out.println("After all tests");
    }

    @Before
    public void beforeEachTest() {
        System.out.println("Before each test");
    }

    @After
    public void afterEachTest() {
        System.out.println("After each test");
    }

    @Test
    public void shouldReturnSumValues() {
        Calculator calculator = new Calculator();

        int sum = calculator.sum(5, 2);

        Assert.assertEquals("Should return 7", 7, sum);
//        Assert.assertTrue(true);
    }

    @Test
    @Ignore
    public void shouldReturnSumValues1() {
        Calculator calculator = new Calculator();

        int sum = calculator.sum(5, 2);

        Assert.assertEquals("Should return 7", 7, sum);
    }

}
