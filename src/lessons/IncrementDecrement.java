package lessons;

public class IncrementDecrement {
    public static void main(String[] args) {
        int count = 2;
        int i;
        /*
        count = count + 1;

        count -= 1;*/

        /*count++;//postincrement
        ++count;//preincrement*/

//        System.out.println(++count);
//        System.out.println(count);

        int r = ++count + count++ + count++ - ++count;

//      3 + 3 + 4 - 6 = 4



        i = 2;

        int d = ++i + ++i + i++ - i++ + ++i - ++i + ++i;

        System.out.println(r);
        System.out.println(d);
    }
}
