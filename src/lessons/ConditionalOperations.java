package lessons;

public class ConditionalOperations {
    public static void main(String[] args) {
        int a = 10;
        int b = 15;

        if (a < 9 && b == 15) {//false && true -> 0 && 1 -> 0
            System.out.println();
        }

        if (a < 9 || b == 15) {//false || true -> 1 -> 0
            System.out.println();
        }

      /*if (a < 10) {
            System.out.println("a < 10");
        } else if (a > 10) {
            System.out.println("a > 10");
        } else {
            System.out.println("a == 10");
        }*/

//        ternary operator
//                condition ? statement if condition is true : statement if condition is false

        int r = a < 10 ? 15 : 20;

//        System.out.println(r);

        int t = a < 10 ? 9 : a > 10 ? 11 : a == 10 ? 10 : 0;

    }
}
