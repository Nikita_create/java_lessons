package lessons.two_dim_array;

import java.util.Random;

public class TwoDimensionArray {
    public static void main(String[] args) {
        /*int[] array = {0, 0, 0};
        int[] array1 = new int[]{1, 2, 3};
        int[] array2 = new int[3];//0*/

        int[][] array = {
                {4, 5, 6},//[0][0]; [0][1]; [0][2]
                {8, 0, 3},//[1][0]; [1][1]; [1][2]
                {7, 2, 1}//[2][0]; [2][1]; [2][2]
        };

        int[][] array1 = {
                {4, 5, 6},
                {8},
                {7, 1}
        };

//        System.out.println(array[1][1]);

        int[][] arr = new int[3][3];//0

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                arr[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(arr[i][j] + "\t");
            }

            System.out.println();
        }

        /*
        * iter0(outer loop): i == 0;
        * iter0(inner loop): j == 0; [0][0]
        * iter1(inner loop): j == 1; [0][1]
        * iter2(inner loop): j == 2; [0][2]
        *
        * iter1(outer loop): i == 1;
        * iter0(inner loop): j == 0; [1][0]
        * iter1(inner loop): j == 1; [1][1]
        * iter2(inner loop): j == 2; [1][2]
        *
        * */


    }
}
