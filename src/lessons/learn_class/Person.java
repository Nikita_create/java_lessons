package lessons.learn_class;

public class Person {

    public static String name = "John";

    {//Выполняется перед конструктором ,а конструктор вызывается при создании объекта
        System.out.println("Non-static block initialization");
    }

    static {//Выполняется при загрузки класса в память
        System.out.println("Static block initialization");
    }

    /*
    * Класс загружается в память в таких случаях:
    * 1) Создание объекта
    * 2) Обращение к статике
    * 3) Вызов psvm метода в этом классе
    * */

    public Person() {
        System.out.println("Constructor");
    }

    public static void main(String[] args) {

    }
}

class TestPerson {
    public static void main(String[] args) {
       /* Person p1 = new Person();
        Person p2 = new Person();*/

//        System.out.println(Person.name);
    }
}
