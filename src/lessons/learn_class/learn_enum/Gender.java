package lessons.learn_class.learn_enum;

public enum Gender {
    MALE("Male"),
    FEMALE("Female");

    private String shortName;

    private Gender(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    /* Gender() {
    }*/
}
