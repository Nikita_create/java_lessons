package lessons.learn_class.learn_enum;

public class LearnEnum {
    public void print(String s) {//"Hello", "mEn"
        //complex logic; s = MAN/WOMAN
    }

    public void print(Gender gender) {

    }
}


class TestEnum {
    public static void main(String[] args) {
        //datatype name_reference = new datatype(params or absent);

        Gender male = Gender.MALE;

        Gender male1 = Gender.MALE;

        LearnEnum learnEnum = new LearnEnum();

        learnEnum.print(Gender.MALE);

//        System.out.println(male == male1);

//        System.out.println(male.getShortName());

        String manGender = "Male";

        Gender gender = Gender.valueOf(manGender.toUpperCase());

        learnEnum.print(gender);

        Gender[] values = Gender.values();

        for (Gender value : values) {
            System.out.println(value);
        }

    }
}
