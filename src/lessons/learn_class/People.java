package lessons.learn_class;

import homeworks.peoples.Gender;
import homeworks.peoples.Position;

public class People {
    private int age;//field age
    private int childs;//field childs
    public final int COUNT_PEOPLE = 6;//non-static constant
    private People man;
    private static String NAME = "John";

    {
//        System.out.println("Non-static block initialization");
//        COUNT_PEOPLE = 6;
    }

    public People(int childs, People man) {
        this.childs = childs;
        this.man = man;
    }

    public People(int age, int childs, People man) {
        this(childs, man);
        this.age = age;
    }

    public People(int newAge, int newChild) {//int newAge = -10;

        System.out.println("Constructor");

//        COUNT_PEOPLE = 6;

        if (newAge < 0) {
            this.age = 0;
        } else {
            age = newAge;
        }
        childs = newChild;
    }

    public People(String viktoria, String mihailov, int age, Gender male, Position worker) {
        this.age = age;
//        COUNT_PEOPLE = 6;

    }


    public People(String newName) {
//        COUNT_PEOPLE = 6;
//        NAME = newName;
    }

    public static String getNAME() {
        return NAME;
    }

    public static void setNAME(String NAME) {
//        this.print();
        People.NAME = NAME;
    }
//Из статического контекста нельзя вызывать нестатический метод, обратное возможно
    public void print() {
        setNAME("");
        this.setAge(45);
//        age = 45;
        System.out.println("Age = " + age + " Childs = " + childs);
    }

    //    access_modifier static(absent) void(or return type) name_method(params or absent)

    public void setAge(int newAge) {//this = m for 68 line
        age = newAge;
    }

    public int getAge() {
        return age;
    }

}


class TestMan {
    public static void main(String[] args) {
        //datatype name_reference = new datatype(params or absent);

        final People m = new People(-10, 1);

        People m4 = new People(5, m);
//        m = new Man(20, 2);
//        Man m3 = m;

//        Man m2 = new Man(50);
//        m.age = -10;
//        m.childs = 1;

        m.setAge(15);

        int countPeople = m.COUNT_PEOPLE;

        int age = m.getAge();

        System.out.println(age);

        People m1 = new People(20, 2);
//        m1.age = 20;
//        m1.childs = 2;

//        m1.setAge(85);

        int a = 50;

        int b = a;
    }
}

class TestStatic {
    public static void main(String[] args) {
   /*
        Man.NAME = null;

        System.out.println(Man.NAME);*/

        People man = new People("John");

//        System.out.println(man.getNAME());

        long start = System.currentTimeMillis();//1604141663488

//        System.out.println(start);

        for (int i = 0; i < 1000000; i++) {
            //new People(i);
        }

        long finish = System.currentTimeMillis();

        System.out.println(finish - start);//1604141663493

        People.setNAME("Ken");

    }
}

