package lessons.learn_class;

/*
Создать класс Банковский счет.
Установить изначально сумму для счета - 200$
Создать метод, который будет отнимать от счета сумму. Если денег недостаточно, тогда написать в консоль - "Пополните счет".
Счет можно пополнить другим методом.
*/

public class BankAccount {
    private int sum;

    public BankAccount() {
        this.sum = 200;
    }

    public void withDrawal(int value) {
        if (value > sum || value <= 0) {
            System.out.println("Incorrect value");
        } else {
            sum -= value;
        }
    }

    public void plus(int value) {
        if (value <= 0) {
            System.out.println("Incorrect value");
        } else {
            sum += value;
        }
    }

    public int getSum() {
        return sum;
    }
}

class TestBankAccount {
    public static void main(String[] args) {
        BankAccount ba = new BankAccount();
        int sum = ba.getSum();//200

        System.out.println(sum + "$");

        ba.plus(300);
        int sumNew = ba.getSum();

        System.out.println(sumNew + "$");

        ba.withDrawal(1050);
        int sumWithDrawal = ba.getSum();

        System.out.println(sumWithDrawal + "$");
    }
}
