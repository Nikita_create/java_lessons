package lessons.learn_class.learn_interface;

import java.io.IOException;
import java.io.Serializable;
import java.nio.CharBuffer;

public class Animal implements Soundable, Serializable, Runnable {

    public void sound() {
        System.out.println("Inside Animal");
    }

    public void run() {

    }

    public void print() {

    }

    @Override
    public int read(CharBuffer cb) throws IOException {
        return 0;
    }
}

class Lion extends Animal {

}

class Mouse extends Animal {

}

interface Soundable extends Cloneable, Readable {
    public void sound();//abstract method(without body)
}


class TestInterface {
    public static void main(String[] args) {
        Readable animal = new Lion();
        
    }
}

