package lessons.learn_string;

/*
* В определенном предложении найти самое длинное и короткое слово.
* */
public class MaxMinWord {
    public static void main(String[] args) {
        String sentence = "Hello my young friend";

        String[] str = sentence.split(" ");

        int maxLength = str[0].length();
        int minLength = str[0].length();

        String maxWord = str[0];
        String minWord = str[0];

        for (String s : str) {
            if (s.length() > maxLength) {
                maxLength = s.length();
                maxWord = s;
            }

            if (s.length() < minLength) {
                minLength = s.length();
                minWord = s;
            }
        }

        System.out.println("Max word is -> " + maxWord);
        System.out.println("Min word is -> " + minWord);
    }
}
