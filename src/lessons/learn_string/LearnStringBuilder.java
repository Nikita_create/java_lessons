package lessons.learn_string;

public class LearnStringBuilder {
    public static void main(String[] args) {
        String sentence = "Hello";

        StringBuilder builder = new StringBuilder(sentence);

        for (int i = 0; i < 5; i++) {
//            sentence = sentence + i;
            builder.append(i);
        }

    }
}
/*
 * iter 0: Hello + 0 -> sentence = Hello0; Hello without ref
 * iter 1: Hello0 + 1 -> sentence = Hello01; Hello0 without ref
 *
 * */