package lessons.learn_string;

public class LearnString {
    public static void main(String[] args) {

        //datatype name_reference = new datatype(params or absent);

        String str = "Hello";//object-literal

        String str1 = new String("Hello");//object

        str1 = str;

        char c = 'H';

        char[] chars = str.toCharArray();//'H', 'e', 'l', 'l', 'o',' '

        str = str + " World";//str -> Hello World

        System.out.println(str.charAt(2));
        System.out.println(str.lastIndexOf('l'));
        System.out.println(str.substring(0, 4));//Hel

        System.out.println(str == str1.intern());
        System.out.println(str == str1);


        System.out.println(str.equals(str1));
        System.out.println(str.equalsIgnoreCase(str1));

        System.out.println("1" + 2 + 3);//"12" + 3 -> "123"
        System.out.println(1 + 2 + "3");//3 + "3" -> "33"

        int m = 9;
        int g = 3;

        String x = "oijeg".concat(String.valueOf(m)) + g;
        System.out.println(x);

    }
}
