package lessons.learn_string;

/*
* Вывести текст, составленный из последних букв всех слов.
* */

public class LastChars {
    public static void main(String[] args) {
        String sentence = "Hello my young friend";

        String[] strings = sentence.split(" ");

        for (String s : strings) {
            System.out.print(s.charAt(s.length() - 1));
        }
    }
}
