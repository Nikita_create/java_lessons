package lessons.exceptions;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.sql.SQLDataException;
import java.util.Arrays;

public class StudyException {

    public static void print() {
//        System.out.println(1 / 0);

//        throw new NullPointerException("I'm NPE");
        try {
            if (1 == 1) {
                throw new IOException("I'm IO");//or code that can throw exception
                //write to unexisting file -> throw exception
            }
        } catch (IOException e) {
//            System.out.println("Inside catch");

//            e.printStackTrace();

            //create file and write to it
        }

        System.out.println("After catch");
    }

    public static void print1() throws IOException {

        if (1 == 1) {

            int[] ints = {1, 2, 3};
            try {
                ints[5] = 8;
            } catch (ArrayIndexOutOfBoundsException e) {
                ints[2] = 8;
            }

            System.out.println(Arrays.toString(ints));
//            throw new IOException("I'm IO");//or code that can throw exception
        }

    }

    public static void print2() {

        try {
            if (1 == 1) {
                throw new IOException();
            } else if (1 != 1) {
                throw new SQLDataException();
            } else {
                throw new Exception();
            }
        } catch (IOException e) {
            System.out.println("Create file");
        } catch (SQLDataException e) {
            System.out.println("Do smth");
        } catch (Exception e) {
            System.out.println("Create file2");
        }

    }

    public static void print4() throws Exception {

        try {
            if (1 == 1) {
                throw new IOException();
            } else if (1 != 1) {
                throw new SQLDataException();
            } else {
                throw new Exception();
            }
        } catch (IOException | SQLDataException e) {
            System.out.println("Create file");
        } catch (Exception e) {
            System.out.println("Create file2");

            e = new FileAlreadyExistsException("");

            throw e;

            /*try {
                throw new FileAlreadyExistsException("");
            } catch (FileAlreadyExistsException e1) {
                e1.printStackTrace();
            }*/
        }

    }

    public static void print5() throws IOException, Exception, SQLDataException {

            if (1 == 1) {
                throw new IOException();
            } else if (1 != 1) {
                throw new SQLDataException();
            } else {
                throw new Exception();
            }
    }

    public static void print3() {

//        System.exit(5);

        try {
            //work with database
            //throw exception
            if (1 == 1) {

                throw new Exception();
            }
        } catch (Exception e) {
            System.out.println("Create file2");
        } finally {
//            System.out.println("Finally");
            //close connection to db
        }
    }

    public static void main(String[] args)  {
        try {
            print5();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
