package lessons;

public class BitOperations {
    public static void main(String[] args){
//        System.out.println(5 % 2);

        System.out.println(Integer.toBinaryString(42));//00101010
        System.out.println(Integer.toBinaryString(15));//00001111
//        System.out.println("42 & 15 = " + (42 & 15));    //00001010
//        System.out.println("42 | 15 = " + (42 | 15));    //00101111
//        System.out.println("42 << 3 = " + (42 << 3));    //00101010 -> 00000001 01010000
//        42 * 2(3) = 336
        System.out.println("42 >> 3 = " + (42 >> 3));    //00101010 -> 00000101
//        42 / 2(3) = 5
     /*   int pow = (int) Math.pow(2, 7);
        System.out.println(pow);
        System.out.println(1 << 7);//1 * 2(7)*/
    }
}

/*
 * 45(10) -> 00101101(2)
 * 45 / 2 = 22, ost - 1
 * 22 / 2 = 11, ost - 0
 * 11 / 2 = 5, ost - 1
 * 5 / 2 = 2, ost - 1
 * 2 / 2 = 1, ost - 0
 * 76543210
 * 00101101(2) -> (10) = 0 * 2(7) + 0 * 2(6) + 1 * 2(5) + 0 * 2(4) + 1 * 2(3) + 1 * 2(2) + 0 * 2(1) + 1 * 2(0) =
 * 32 + 8 + 4 + 1 = 45
 *
 * 478(10) -> 736(8)
 * 478 / 8 = 59, ost = 6
 * 59 / 8 = 7, ost = 3
 * 210
 * 736(8) -> 7 * 8(2) + 3 * 8(1) + 6 * 8(0) = 448 + 24 + 6 = 478
 *
 * 4830(10) -> 12DE(16)
 *
 * 4830 / 16 = 301, ost - 14
 * 301 / 16 = 18, ost - 13
 * 18 / 16 = 1, ost - 2
 *
 *  * */
