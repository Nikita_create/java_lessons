package lessons;

import java.util.Scanner;

public class LearnSwitch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Type number");

        int value = scanner.nextInt();

        /*if (value == 10) {
            System.out.println("You typed 10");
        }
        if (value == 11) {
            System.out.println("You typed 11");
        }
        if (value == 12) {
            System.out.println("You typed 12");
        }
        if (value == 13) {
            System.out.println("You typed 13");
        }*/

        switch (value) {
            case 10:
                System.out.println("You typed 10");
                break;
            case 11:
                System.out.println("You typed 11");
                break;
            case 12:
                System.out.println("You typed 12");
                break;
            case 13:
                System.out.println("You typed 13");
                break;
            default:
                System.out.println("Unknow number");

        }

    }
}
