package lessons;

public class PrimitiveDataType {
    public static void main(String[] args) {
        byte b = 12;
        byte d = 12;

        byte c = (byte) (b + 1);//byte(8) + int - > int(32)

        byte r = 12 + 12;//24

        int a = b;

        long g = 45L;

        g = 78;

        boolean n = true;

        char f = '\u00A5';
        char f1 = 'R';
        char f2 = 165;

        float q = 12.45F;

        double v = 45.78;


        System.out.println(f2);
    }
}
