package lessons;

public class Loops {
    public static void main(String[] args) {
        /*
         * for
         * while
         * do-while
         * for(foreach)
         * */

        /*for (int i = 0; i <= 10; ++i) {
            System.out.println(i);
        }*/

        int i = 11;

        while (i++ <= 10) {
            System.out.println(i);
            //          i++;
        }

        int r = 0;

        /*do {
            System.out.println(r);
            r++;
        } while (r <= 10);*/

//        for (;;);

//        while (true);
    }
}
