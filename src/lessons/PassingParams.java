package lessons;

public class PassingParams {

    public static void passPrimitives(int a) {
        a = 15;
    }

    public static void passObject(Dog dog) {
        dog.weight = 55;
//        dog.name = new char[]{'F'};
    }

    public static void passString(String s) {
        s = "World";//new String("World")
    }



    public static void main(String[] args) {
        int b = 10;

        passPrimitives(b);//copy

//        System.out.println(b);

        Dog dog = new Dog();

        passObject(dog);

//        System.out.println(dog.weight);

        String str = "Hello";

        passString(str);
    }
}

class Dog {
    public int weight = 45;
    final char[] name = null;
}
