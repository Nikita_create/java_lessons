package lessons.for_test;

public class Car {
    private Wheel wheel;

    public Car(Wheel wheel) {
        this.wheel = wheel;
    }

    public int getWheelSize() {
        return wheel.size();
    }
}
