package lessons.for_test;


import java.util.Random;

public class Calculator {

    public static final Random RANDOM = new Random();

    public int sum(int a, int b) {
        return a + b;
    }

    public void print() {
        System.out.println("Hello");
    }

    public int getRandomValue() {
        return RANDOM.nextInt(20);
    }

    public int getSecondValue() {
        return RANDOM.nextInt(20);
    }

    private int getFirstRandomPrivate() {
        return RANDOM.nextInt(20);
    }

    private int getSecondRandomPrivate() {
        return RANDOM.nextInt(20);
    }

    private int getRandomPrivate() {
        return RANDOM.nextInt(20);
    }

    public int sumRandom(int a) {
        print();
        return a + getRandomValue();
    }

    public int randomSum() {
        return getRandomValue() + getSecondValue();
    }

    public int randomMultiplication() {
        return getRandomValue() * getSecondValue();
    }

    public int randomDivision() {
        return getRandomValue() / getSecondValue();
    }

    public int randomMinus() {
        return getRandomValue() - getSecondValue();
    }

    public int randomDivisionWithRemainder() {
        return getRandomValue() % getSecondValue();
    }

    public int sumRandomPrivate(int a) {
        return a + getRandomPrivate();
    }

    public static int getRandomStatic() {
        return RANDOM.nextInt(20);
    }

    public static int getRandomStaticSecond() {
        return RANDOM.nextInt(20);
    }

    public int randomStaticSum() {
        return getRandomStatic() + getRandomStaticSecond();
    }

    public int randomStaticMultiplication() {
        return getRandomStatic() * getRandomStaticSecond();
    }

    public int randomStaticDivision() {
        return getRandomStatic() / getRandomStaticSecond();
    }

    public int randomStaticMinus() {
        return getRandomStatic() - getRandomStaticSecond();
    }

    public int randomStaticDivisionWithRemainder() {
        return getRandomStatic() % getRandomStaticSecond();
    }


    public int randomPrivateSum() {
        return getFirstRandomPrivate() + getSecondRandomPrivate();
    }

    public int randomPrivateMultiplication() {
        return getFirstRandomPrivate() * getSecondRandomPrivate();
    }

    public int randomPrivateDivision() {
        return getFirstRandomPrivate() / getSecondRandomPrivate();
    }

    public int randomPrivateMinus() {
        return getFirstRandomPrivate() - getSecondRandomPrivate();
    }

    public int randomPrivateDivisionWithRemainder() {
        return getFirstRandomPrivate() % getSecondRandomPrivate();
    }

    public static int getFirstRandomStatic() {
        return RANDOM.nextInt(20);
    }

    public static int getSecondRandomStatic() {
        return RANDOM.nextInt(20);
    }

    public int returnStaticSum() {
        return getFirstRandomPrivate() + getSecondRandomPrivate();
    }

}

/*
 * A(int c) {//10
 *   c + B(c);//30
 * }
 * private B(int a) -> a * 2
 *
 * public D {
 *   C()
 * }
 * public C
 *
 * */
