package lessons;

import java.util.Scanner;

public class WorkWithConsole {
    public static void main(String[] args) {
//        System.out.println();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Type your name");

//        String text = scanner.next();

//        System.out.println("Your name is " + text);

        int i = scanner.nextInt();

        System.out.println("Input -> " + i);

    }
}
