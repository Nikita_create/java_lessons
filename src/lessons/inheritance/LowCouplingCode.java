package lessons.inheritance;

public class LowCouplingCode {
    private One one;
    private Two two;

    public LowCouplingCode(One one) {
        this.one = one;
    }

    public void action() {
        one.print();
    }
}

class NewLowCouplingCode {
    private Printable printable;//association

    public NewLowCouplingCode(Printable printable) {//aggregation
        this.printable = printable;
    }

    public NewLowCouplingCode() {//composition
        this.printable = new Two();
    }

    public void action() {
        printable.print();
    }
}

interface Printable {
    void print();
}

class One implements Printable {
    public void print() {
        System.out.println("One");
    }
}

class Two implements Printable {
    public void print() {
        System.out.println("Two");
    }
}

class TestCouplingCode {
    public static void main(String[] args) {
        Printable one = new One();
        Printable two = new Two();

        NewLowCouplingCode code = new NewLowCouplingCode(two);//copy

        code.action();
    }
}
