package lessons.inheritance;

public class Polymorphism {
    public static void main(String[] args) {
        Fish fish = new FlyFish();//new Fish; later coupling

        fish.action();

//        int a = "";
    }
}

class Fish {
    public void action() {
        System.out.println("Some action");
    }

    public void action(String value) {
        System.out.println(value);
    }


}

class FlyFish extends Fish {
    public void action() {
        System.out.println("I can fly");
    }
}

class JumpFish extends Fish {
    public void action() {
        System.out.println("I can jump");
    }
}
