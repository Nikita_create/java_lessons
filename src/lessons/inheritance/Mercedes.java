package lessons.inheritance;

public class Mercedes extends Car {//subclass, child-class
    private String modelMercedes;

    {
        System.out.println("Non-static block initialization Mers - 4");
    }

    static {
        System.out.println("Static block initialization Mers - 5");
    }

    public Mercedes(String name, int age, String modelMercedes) {
        super(name, age);
        System.out.println("Constructor Mers - 6");
        this.modelMercedes = modelMercedes;
    }

    public String getModelMercedes() {
        return modelMercedes;
    }
}

class Bmw extends Car {//is - a
    private String modelBmw;//has - a

    public Bmw(String name, int age, String modelBmw) {
        super(name, age);
        this.modelBmw = modelBmw;
    }
}

//2 5 1 3 4 6
class TestInheritance {
    public static void main(String[] args) {
        Mercedes mercedes = new Mercedes("Merc", 10, "E200");

        Car car = new Car();

//        car.age;

    }
}