package lessons.inheritance.learn_interface;

import lessons.inheritance.Car;

import java.io.File;
import java.io.Serializable;

public abstract class Animal implements Soundable, Cloneable {

    public int age;

    Object print(int value) {
        return new Car();
    }

    private File print() {
        return null;
    }

    public String print(int value, float b) {
        System.out.println("" + age);

        fly();

        return null;
    }

    public abstract void clear();

    public void fly() {

    }

   /* @Override
    public void makeSound() {

    }

    @Override
    public void run() {

    }*/
}

interface Soundable extends Serializable, Runnable {

    public static final int COUNT = 5;

    void makeSound();

    default void view() {
        System.out.println();
    }

    static void fun() {

    }
}

/*
 * private - visibility only inside class
 * package - private - visibility only inside package
 * protected - visibility only inside package + subclasses
 * public - visibility anywhere
 * */
