package lessons.inheritance;

public class Car {//super-class, parent-class
    private String name;
    private int age;

    {
        System.out.println("Non-static block initialization Parent - 1");
    }

    static {
        System.out.println("Static block initialization Parent - 2");
    }

    public Car(String name, int age) {
        System.out.println("Constructor Parent - 3");
        this.name = name;
        this.age = age;
    }

    public Car() {
    }

    public String getName() {
        return name;
    }

    private int getAge() {
        return age;
    }

    public static void main(String[] args) {
        Car car = new Car();
    }
}
