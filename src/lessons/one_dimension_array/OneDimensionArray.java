package lessons.one_dimension_array;

import java.util.Random;

public class OneDimensionArray {
    public static void main(String[] args) {
//        datatype[] name = new datatype[size];

        int[] array = new int[6];//0

        array[0] = 45;
        array[1] = 12;
        array[2] = 7;
        array[3] = 0;
        array[4] = 8;
        array[5] = 3;

//        System.out.println(array[0]);

        /*for (int i = 0; i < 6; i++) {
            System.out.print(i + "\t");
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }*/

        /*for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }*/

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(50);
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }
    }
}

/*
* iter0: i == 0; array[0] = 0 + 1
* iter1: i == 1; array[1] = 1 + 1
* */
