package lessons.one_dimension_array;

import java.util.Random;

public class LearnForeach {
    public static void main(String[] args) {
        int[] array = new int[12];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(12) + 7;
        }

        /*for (int i = 0; i < array.length; i++) {

            int element = array[i];

            System.out.println(element);
        }*/

        for (int e : array) {
            System.out.print(e + "\t");
        }

    }
}
