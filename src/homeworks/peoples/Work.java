package homeworks.peoples;

public class Work {

    private People[] men;

    public Work() {
        men = new People[10];//nulls
    }

    public void addMan(People man) {
        for (int i = 0; i < men.length; i++) {
            if (men[i] == null) {
                men[i] = man;
                break;
            }
        }
    }

    public void printInfoAllMen() {
        for (People m : men) {
            if (m != null) {
                m.view();
            }
        }
    }

    public void checkPositionAllMen() {
        for (int i = 0; i < men.length; i++) {
            for (int j = i + 1; j < men.length; j++) {
                if (men[i] != null && men[j] != null &&
                        men[i].getPosition().getName().equals(men[j].getPosition().getName())) {
                    men[i].view();
                    men[j].view();
                }
            }
        }
    }

//"One", "Two", "Three"

    public void checkLastNameAllMen() {

        int index = 0;

        People[] people = new People[men.length];

        for (int m = 0; m < men.length; m++) {
            for (int s = m + 1; s < men.length; s++) {
                if (men[m] != null && men[s] != null &&
                        men[m].getLastName().equals(men[s].getLastName())) {
//                    men[m].view();
//                    men[s].view();
                    for (int i = 0; i < people.length; i++) {
                        if (people[i] != null && !people[i].getLastName().equals(men[m].getLastName()) &&
                                !people[i].getFirstName().equals(men[m].getFirstName())) {
                            people[index] = men[m];
                            index++;
                        }
                    }

                }
            }
        }

        for (People person : people) {
            if (person != null) {
                person.view();
            }
        }
    }
}

class TestWork {
    public static void main(String[] args) {
        Work work = new Work();

        Position director = new Position("Director");
        Position accountant = new Position("Accountant");
        Position worker = new Position("Worker");

        People man = new People("Oleg", "Zaharov", 32, Gender.MALE, director);
        People man1 = new People("Olga", "Pertoh", 29, Gender.MALE, worker);
        People man2 = new People("Maxim", "Pertoh", 35, Gender.MALE, accountant);
        People man3 = new People("Viktoria", "Mihailov", 28, Gender.MALE, worker);
        People man4 = new People("Oleg", "Pertoh", 35, Gender.MALE, accountant);

        work.addMan(man);
        work.addMan(man1);
        work.addMan(man2);
        work.addMan(man3);
        work.addMan(man4);

//        work.printInfoAllMen();

//        System.out.println();
        work.checkLastNameAllMen();

        /*System.out.println();
        work.checkPositionAllMen();*/
    }
}


