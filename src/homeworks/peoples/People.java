package homeworks.peoples;

public class People {
    private String firstName;
    private String lastName;
    private int age;
    private Gender gender;
    private Position position;

    public People(String firstName, String newLastName, int newAge, Gender newGender, Position newPosition) {
        this.firstName = firstName;
        this.lastName = newLastName;
        this.age = newAge;
        this.gender = newGender;
        this.position = newPosition;
    }

    public void view() {
        String info = "Name = " + this.firstName + "\tLast name = " + this.lastName + "\tAge = " +
                this.age + "\tGender = " + this.age + "\tPosition = " + this.position.getName();
        System.out.println(info);
    }

    public Position getPosition() {
        return position;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }
}