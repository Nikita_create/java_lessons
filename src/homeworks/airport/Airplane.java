package homeworks.airport;

public class Airplane {
    private int timeInAir;
    private int numberOfSeats;
    private double time;
    private int numberOfAir;
    private DeparturePoint departurePoint;
    private PointOfArrival pointOfArrival;

    public Airplane(double newTime, int newNumberOfAir, DeparturePoint newDeparturePoint,
                    PointOfArrival newPointOfArrival, int newTimeInAir, int newNumberOfSeats) {
        this.departurePoint = newDeparturePoint;
        this.numberOfAir = newNumberOfAir;
        this.pointOfArrival = newPointOfArrival;
        this.time = newTime;
        this.timeInAir = newTimeInAir;
        this.numberOfSeats = newNumberOfSeats;
    }

    public int getTimeInAir() {
        return timeInAir;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public DeparturePoint getDeparturePoint() {
        return departurePoint;
    }

    public PointOfArrival getPointOfArrival() {
        return pointOfArrival;
    }

    public void view() {
        String info = "Number of air = " + this.numberOfAir + "\tTime = " + this.time + "\tDeparturePoint = "
                + this.departurePoint + "\tPointOfArrival = " + this.pointOfArrival;
        System.out.println(info);
    }
}
