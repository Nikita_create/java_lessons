package homeworks.airport;

public enum DeparturePoint {
    KYIV,
    MALIBU,
    THAILAND,
    MOSCOW,
    LONDON
}
