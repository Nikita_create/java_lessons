package homeworks.airport;

public enum PointOfArrival {
    KYIV,
    MALIBU,
    THAILAND,
    MOSCOW,
    LONDON
}
