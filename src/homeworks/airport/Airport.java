package homeworks.airport;

public class Airport {
    private Airplane[] airs;

    public Airport() {
        airs = new Airplane[10];
    }

    public void addAirs(Airplane air) {
        for (int i = 0; i < airs.length; i++) {
            if (airs[i] == null) {
                airs[i] = air;
                break;
            }
        }
    }

    public void printAllAirs() {
        for (int i = 0; i < airs.length; i++) {
            if (airs[i] != null) {
                airs[i].view();
            }
        }
    }

    public void searchAirByPointOfArrival(PointOfArrival pointOfArrival) {
        for (int i = 0; i < airs.length; i++) {
            if (airs[i] != null && airs[i].getPointOfArrival().equals(pointOfArrival)) {
                airs[i].view();
            }
        }
    }

    public void searchAir(DeparturePoint departurePoint , PointOfArrival pointOfArrival) {
        for (int i = 0; i < airs.length; i++) {
            if (airs[i] != null &&
                    airs[i].getPointOfArrival() == pointOfArrival &&
                    airs[i].getDeparturePoint() == departurePoint) {
                airs[i].view();
            }
        }
    }

    public void cleverSearch(DeparturePoint departurePoint, PointOfArrival pointOfArrival,
                             int timeInAir, int numberOfSeats) {
        for (int i = 0; i < airs.length; i++) {
            if (airs[i] != null && airs[i].getDeparturePoint().equals(departurePoint)
                    && airs[i].getPointOfArrival().equals(pointOfArrival)
                    && airs[i].getNumberOfSeats() == numberOfSeats
                    && airs[i].getTimeInAir() == timeInAir) {
                airs[i].view();
            }
        }
    }
}
