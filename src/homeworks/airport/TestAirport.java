package homeworks.airport;

public class TestAirport {
    public static void main(String[] args) {
        Airport airs = new Airport();

        Airplane air = new Airplane( 25.12, 231,DeparturePoint.KYIV,PointOfArrival.MOSCOW,
                2,25);
        Airplane air1 = new Airplane( 15.21, 375,DeparturePoint.THAILAND,PointOfArrival.LONDON,
                6,45);
        Airplane air2 = new Airplane( 05.14, 178,DeparturePoint.LONDON,PointOfArrival.KYIV,
                8, 25);
        Airplane air3 = new Airplane( 20.11, 802,DeparturePoint.MALIBU,PointOfArrival.THAILAND,
                6,20);

        airs.addAirs(air);
        airs.addAirs(air1);
        airs.addAirs(air2);
        airs.addAirs(air3);

        airs.printAllAirs();

            System.out.println();
        airs.searchAirByPointOfArrival(PointOfArrival.LONDON);

            System.out.println();
        airs.searchAir(DeparturePoint.LONDON, PointOfArrival.KYIV);

            System.out.println();
        airs.cleverSearch(DeparturePoint.KYIV, PointOfArrival.MOSCOW,2,25);
    }
}
