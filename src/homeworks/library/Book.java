package homeworks.library;

import java.time.LocalDate;

public class Book {
    private String author;
    private String name;
    private Genre genre;
    private String userName;
    private LocalDate date;

    public Book(String author, String name, Genre genre) {
        this.author = author;
        this.name = name;
        this.genre = genre;
        this.userName = null;
    }

    public void view() {
        String info = "This author = " + this.author + "\tName of book = " +
                this.name + "\tThis Genre = " + this.genre + "\tBook gives to " + this.userName;
        System.out.println(info);
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
