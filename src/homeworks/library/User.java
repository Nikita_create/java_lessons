package homeworks.library;

public class User {
    private String name;
    private boolean isAdmin;
    private int password;

    public User(String name, boolean isAdmin, int password) {
        this.name = name;
        this.isAdmin = isAdmin;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public int getPassword() {
        return password;
    }

    public void view() {
        System.out.println("Name = " + name + "\tIs admin = " + isAdmin + "\tPassword = " + password);
    }
}
