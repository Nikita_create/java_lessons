package homeworks.library;

import java.util.Scanner;

public class LibraryMenu {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        Library library = new Library();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            switch (key) {
                case 1: {

                    System.out.println("Enter name");

                    String name = SCANNER.next();

                    System.out.println("Is admin");

                    boolean isAdmin = SCANNER.nextBoolean();

                    System.out.println("Enter password");

                    int password = SCANNER.nextInt();

                    User user = new User(name, isAdmin, password);

                    library.addUser(user);

                    break;
                }
                case 2: {

                    library.showAllUsers();

                    break;
                }
                case 3: {

                    String name = getInfo("Enter name");

                    String admin = getInfo("Is admin");

                    boolean isAdmin = Boolean.parseBoolean(admin);

                    int password = Integer.parseInt(getInfo("Enter password"));

                    User user = new User(name, isAdmin, password);

                    library.addUser(user);


                    break;
                }
                case 10:
                    return;
            }
        }
    }

    private static String getInfo(String message) {
        System.out.println(message);

        return SCANNER.next();
    }

    public static void menu() {
        System.out.println(
                "1) Add user\n" +
                "2) Show all users\n" +
                "10) Exit.");
    }
}

