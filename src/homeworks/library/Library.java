package homeworks.library;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Scanner;

public class Library {
    private User[] users;
    private Book[] library;

    public Library() {
        users = new User[10];

        users[0] = new User("Jane", true, 132);
        users[1] = new User("Dan", false, 456);
        users[2] = new User("Carl", false, 789);

        library = new Book[10];
    }

    public void addUser(User user) {
        for (int i = 0; i < users.length; i++) {
            if (Objects.isNull(users[i])) {
                users[i] = user;
                break;
            }
        }
    }

    public void showAllUsers() {
        for (int i = 0; i < users.length; i++) {
            if (Objects.nonNull(users[i])) {
                users[i].view();
            }
        }
    }

    private boolean existUser(String userName) {
        for (User user : users) {
            if (user.getName().equals(userName)) {
                return true;
            }
        }

        return false;
    }

    private boolean existBook(String bookName) {
        for (Book book : library) {
            if (Objects.nonNull(book) && book.getName().equals(bookName)) {
                return true;
            }
        }

        return false;
    }

    public void giveBook(Book book, int year, int month, int dayOfMonth, String userName) {
        if (existUser(userName) && existBook(book.getName())) {
            book.setUserName(userName);

            book.setDate(LocalDate.of(year, month, dayOfMonth));
            System.out.println("Book has been issued to " + userName);
        }
    }

    public void returnBook(Book book, String userName) {
        if (existUser(userName) && existBook(book.getName())) {
            book.setUserName(null);

            LocalDate now = LocalDate.now();
            System.out.println("Book has been returned after " + (now.getDayOfMonth() -
                    book.getDate().getDayOfMonth() + " days" +
                    "\tBy the " + userName));
        }
    }




    public void addBook(Book book, int userPassword, String user) {
        for (User u : users) {
            if (u != null && user.equals(u.getName())) {
                if (userPassword == u.getPassword() && u.isAdmin()) {
                    for (int i = 0; i < library.length; i++) {
                        if (library[i] == null) {
                            library[i] = book;
                            break;
                        }
                    }
                } else {
                    System.out.println("You not admin");
                }
            }
        }

    }

    public void deleteBook(Book book, int userPassword) {
        for (User u : users) {
            if (Objects.nonNull(u) && userPassword == u.getPassword()) {
                for (int i = 0; i < library.length; i++) {
                    if (library[i] != null && book.getAuthor().equals(library[i].getAuthor()) &&
                            book.getName().equals(library[i].getName())) {
                        library[i] = null;
                    }
                }
            }
        }
    }

    public void showLibrary() {
        for (Book s : library) {
            if (s != null) {
                s.view();
            }
        }
    }

}

