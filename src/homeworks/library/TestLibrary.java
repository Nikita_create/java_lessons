package homeworks.library;

class TestLibrary {
    public static void main(String[] args) {
        Library library = new Library();

        Book book1 = new Book("Оскар Уайльд", "Портрет Дориана Грея", Genre.FANTASY); // Фэнтэзи
        Book book2 = new Book("Николай Гоголь", "Шинель", Genre.FANTASY); // Художественный вымысел
        Book book3 = new Book("Николай Гоголь", "Тарас Бульба", Genre.ROMAN); // Любовный роман, истор. жанр, новелла
        Book book4 = new Book("Михаил Лермонтов", "Герой нашего времени", Genre.ROMAN); // Роман
        Book book5 = new Book("Михаил Лермонтов", "Мцыри", Genre.DRAMA); // Поэма
        Book book6 = new Book("Пауло Коэльо", "Алхимик", Genre.DRAMA); // Драмма

        library.addBook(book1,132,"Jane");
        library.addBook(book2,132,"Jane");
        library.addBook(book3,132,"Jane");
        library.addBook(book4,132,"Jane");
        library.addBook(book5,132,"Jane");
        library.addBook(book6,132,"Jane");

        library.showLibrary();

        library.deleteBook(book1 ,132);

        library.giveBook(book2,2020,11,3, "Igor");

        library.returnBook(book2, "Jane");



    }
}
