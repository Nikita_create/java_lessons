package homeworks.home_string_type;

/*
Count chars of strings placed in the odd positions in the given array. For example: array = ["Hello", "world",
"from", "array"] the result will be 9: "Hello" length is 5 and "from" length is 4 "world" and "array" are skipped
because they are on the even positions.
*/

public class HomeWork1 {
    public static void main(String[] args) {
        String sentence = "My mom accidentally come into my room";

        String[] str = sentence.split(" ");

        int anOddWordsIndex = 0; // The number of letters in words with an odd index

        for (int i = 0; i < str.length; i++) {
            if (i % 2 == 1) {
                anOddWordsIndex += str[i].length();
            }
        }

        System.out.println("\nThe number of letters in words with an odd index = " + anOddWordsIndex);
    }
}
