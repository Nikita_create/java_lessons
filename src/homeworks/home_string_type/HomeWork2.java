package homeworks.home_string_type;

// Подсчитать количество слов в предложении.

public class HomeWork2 {
    public static void main(String[] args) {
        String sentence = "Hello my dear friend";

        String[] strings = sentence.split(" ");

        System.out.println("In the sentence ' " + sentence + " ' - " + strings.length + " words ");
    }
}
