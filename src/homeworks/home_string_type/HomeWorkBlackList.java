package homeworks.home_string_type;

public class HomeWorkBlackList {
    public static void main(String[] args) {
        String sentence = "Hello m og fried";
        String str = "uhi";

        Method m = new Method();

//        m.BLACK_LIST

        boolean isLetters = m.hasNoneLetters(str);

        System.out.println(isLetters);
    }
}

class Method{
    public final String BLACK_LIST = "hyun";

    public boolean hasNoneLetters(String sentence) {

        for (int i = 0; i < sentence.length(); i++) {
            char symbol = sentence.charAt(i);//'D'

            String s = String.valueOf(symbol);//"D"

            if (BLACK_LIST.contains(s.toLowerCase())) {
                return false;
            }
        }

        return true;
    }
}
