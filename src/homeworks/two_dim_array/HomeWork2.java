package homeworks.two_dim_array;

//2) В двумерном массиве натуральных случайных чисел от 10 до 99 найти количество всех двухзначных чисел, у которых
// сумма цифр кратная 2.

import java.util.Random;

public class HomeWork2 {
    public static void main(String[] args) {
        int[][] array = new int[6][6];

        Random random = new Random();

        for (int i = 0; i<array.length;i++){
            for (int j = 0; j < array.length; j++) {
                array[i][j] = random.nextInt(99 - 10 + 1) + 10;
            }
        }

        for (int i = 0; i<array.length;i++){
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] +"\t");
            }
            System.out.println();
        }

        int score = 0;

        for (int i = 0; i<array.length;i++){
            for (int j = 0; j < array.length; j++) {
                int x = array[i][j] / 10  + array[i][j] % 10;
                if (x % 2 == 0){
                    score++;
                }
            }
        }

        System.out.println();
        System.out.println("Number whose sum of digits is a multiple of 2 = " + score);
    }
}
