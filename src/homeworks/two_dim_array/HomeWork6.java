package homeworks.two_dim_array;

import java.util.Random;

/*5) В двумерном массиве Необходимо найти и вывести наибольший, наименьший элемент, сумму всех элементов
каждого столбца.*/

public class HomeWork6 {
    public static void main(String[] args) {
        int[][] array = new int[5][5];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }


        for (int i = 0; i < array.length; i++) {

            int max = array[0][i];
            int min = array[0][i];
            int sum = 0;

            for (int j = 0; j < array[i].length; j++) {
                if (array[j][i] > max) {
                    max = array[j][i];
                }

                if (array[j][i] < min) {
                    min = array[j][i];
                }

                sum += array[j][i];
            }

            System.out.println("Max number of " + i + " column = " + max);
            System.out.println("Min number of " + i + " column = " + min);
            System.out.println("Amount of " + i + " column = " + sum);
        }


    }
}
