package homeworks.two_dim_array;

import java.util.Random;

/*
* 5) В двумерном массиве Вывести максимальный элемент каждой строки/каждого столбца
* */
public class Homework1 {
    public static void main(String[] args) {
        int[][] array = new int[4][4];//0

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }

            System.out.println();
        }

        int max1 = array[0][0];

        for (int i = 0; i < array.length; i++) {

            int max = array[i][0];

            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }
            }

            System.out.println("Max element of " + i + " row = " + max);
        }

        for (int i = 0; i < array[0].length; i++) {

            int max = array[0][i];

            for (int j = 0; j < array.length; j++) {
                if (array[j][i] > max) {
                    max = array[j][i];
                }
            }

            System.out.println("Max element of " + i + " column = " + max);
        }
    }
}
