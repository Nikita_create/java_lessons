package homeworks.two_dim_array;

//3) Вывести на экран среднее арифметическое каждого строки/столбца двумерного массива.

import java.util.Random;

public class HomeWork3 {
    public static void main(String[] args) {
        int[][] array = new int[4][4];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        System.out.println();

        int sum = 0;

        int y = 0;

        double avgRow = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
            avgRow = sum * 1.0 / 4;
            sum = 0;
            System.out.println("On field " + y + " arithmetic all numbers = " + avgRow);;
        }

        int sumColumn1 = 0;
        int sumColumn2= 0;
        int sumColumn3 = 0;
        int sumColumn4 = 0;
        double avgColumn1;
        double avgColumn2;
        double avgColumn3;
        double avgColumn4;

        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++) {
                if (j == 0) {
                    sumColumn1 += array[i][j];
                }

                if (j == 1) {
                    sumColumn2 += array[i][j];
                }

                if (j == 2) {
                    sumColumn3 += array[i][j];
                }

                if (j == 3) {
                    sumColumn4 += array[i][j];
                }

            }
        }

        avgColumn1 = sumColumn1 * 1.0 / 4;
        avgColumn2 = sumColumn2 * 1.0 / 4;
        avgColumn3 = sumColumn3 * 1.0 / 4;
        avgColumn4 = sumColumn4 * 1.0 / 4;

        System.out.println(avgColumn1);
        System.out.println(avgColumn2);
        System.out.println(avgColumn3);
        System.out.println(avgColumn4);
    }
}
