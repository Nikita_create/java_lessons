package homeworks.two_dim_array;

import java.util.Queue;
import java.util.Random;

//4) Вычислить сумму элементов двумерного массива входящих в главную диагональ. i == j

public class HomeWork4 {
    public static void main(String[] args) {
        int[][] array = new int[4][4];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();

        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == j) {
                    sum += array[i][j];
                }
            }
        }

        for (int i = 0; i < array.length; i++) {
            sum += array[i][i];
        }

        System.out.println(sum);
    }
}
