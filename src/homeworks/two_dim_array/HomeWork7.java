package homeworks.two_dim_array;

import java.util.Random;

public class HomeWork7 {
    public static void main(String[] args) {
        int[][] array = new int[4][4];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        int sumFirst = 0;
        int sumLast = 0;

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (i == 0) {
                    sumFirst += array[i][j];
                }

                if (j == array.length - 1) {
                    sumLast += array[i][j];
                }
            }
        }

        for (int i = 0; i < array.length; i++) {
            sumFirst += array[0][i];
            sumLast += array[array.length - 1][i];
        }

        System.out.println("sumFirst = " + sumFirst);
        System.out.println("sumLast = " + sumLast);
    }
}
