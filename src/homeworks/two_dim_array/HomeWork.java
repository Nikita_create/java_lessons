package homeworks.two_dim_array;

//1) В двумерном массиве целых чисел определить, сколько раз в нем встречается элемент со значением X.

import java.util.Random;

public class HomeWork {
    public static void main(String[] args) {
        int[][] array = new int[4][4];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = random.nextInt(20);
            }
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }

        int counter = 0;
        int x = random.nextInt(20);

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] == x) {
                    counter++;
                }
            }
        }
        System.out.println("Number " + x + " occurs " + counter + "  Times");
    }
}
