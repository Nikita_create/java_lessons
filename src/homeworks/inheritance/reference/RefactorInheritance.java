package homeworks.inheritance.reference;

import java.util.Scanner;

class Reference {
    String author1;
    String author2;
    String author3;
    String author4;
    Boolean isUpdated;
    int year;
    int volume;

    public Reference(String author1, String author2, String author3,
                     String author4, Boolean isUpdated, int year, int volume) {
        this.author1 = author1;
        this.author2 = author2;
        this.author3 = author3;
        this.author4 = author4;
        this.isUpdated = isUpdated;
        this.year = year;
        this.volume = volume;
    }
}

class Library {
    Book[] books;
    Article[] articles;

    public Library() {
        books = new Book[10];
        articles = new Article[10];
    }

    public void addBook(Book book) {
        for (int i = 0; i < books.length; i++) {
            if (books[i] == null) {
                books[i] = book;
                break;
            }
        }
    }

    public void addArticle(Article article) {
        for (int i = 0; i < articles.length; i++) {
            if (articles[i] == null) {
                articles[i] = article;
                break;
            }
        }
    }

    public void shoAllBooks() {
        for (Book s : books) {
            if (s != null) {
                s.view();
            }
        }
    }

    public void showAllArticles() {
        for (Article s : articles) {
            if (s != null) {
                s.view();
            }
        }
    }
}

class Book extends Reference {
    int countOfPages;
    String nameBook;

    public Book(String author1, String author2, String author3,
                String author4, Boolean isUpdated, int year, int volume, int countOfPages, String nameBook) {
        super(author1, author2, author3, author4, isUpdated, year, volume);
        this.countOfPages = countOfPages;
        this.nameBook = nameBook;
    }

    //  do something with book
    public int getCountOfPages() {
        return countOfPages;
    }

    public String getNameBook() {
        return nameBook;
    }

    public Boolean getIsUpdate() {
        return isUpdated;
    }

    public String getAuthor1() {
        return author1;
    }

    public void view() {
        String info = "This name of book : " + this.nameBook + ";" + " This count of pages - " +
                this.countOfPages + ";" + " This author : " + this.author1 + ";" +
                " Is Update = " + this.isUpdated + ";";
        System.out.println(info);

    }
}


class Article extends Reference {
    private int startPage;
    private int endPage;
    private String nameArticle;

    public Article(String author1, String author2, String author3, String author4,
                   Boolean isUpdated, int year, int volume, int startPage, int endPage, String nameArticle) {
        super(author1, author2, author3, author4, isUpdated, year, volume);
        this.startPage = startPage;
        this.endPage = endPage;
        this.nameArticle = nameArticle;
    }
    //        do something with article

    public void view() {
        String info = "This name of article : " + this.nameArticle + ";" + "this author - " + this.author1 + ";" +
                "Is Updated = " + this.isUpdated + ";";
        System.out.println(info);
    }
}

class TestInheritance {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        Library library = new Library();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            switch (key) {
                case 1: {
                    System.out.println("Name of book");
                    String name = SCANNER.next();

                    System.out.println("Count of pages");
                    int countOfPages = SCANNER.nextInt();

                    System.out.println("Enter name of first author");
                    String author1 = SCANNER.next();

                    System.out.println("Enter name of second author");
                    String author2 = SCANNER.next();

                    System.out.println("Enter name of third author");
                    String author3 = SCANNER.next();

                    System.out.println("Enter name of fours author");
                    String author4 = SCANNER.next();


                    System.out.println("year of writing");
                    int year = SCANNER.nextInt();

                    System.out.println("It's volume");
                    int volume = SCANNER.nextInt();

                    System.out.println("Is update ?");
                    boolean isUpdate = SCANNER.nextBoolean();

                    Book book = new Book(author1, author2, author3, author4, isUpdate, year,
                            volume, countOfPages, name);

                    library.addBook(book);
                    break;
                }
                case 2: {
                    System.out.println("Name of article");
                    String name = SCANNER.next();

                    System.out.println("Start page");
                    int startPage = SCANNER.nextInt();

                    System.out.println("End page");
                    int endPage = SCANNER.nextInt();

                    System.out.println("Enter name of first author");
                    String author1 = SCANNER.next();
                    System.out.println("Enter name of second author");
                    String author2 = SCANNER.next();
                    System.out.println("Enter name of third author");
                    String author3 = SCANNER.next();
                    System.out.println("Enter name of fours author");
                    String author4 = SCANNER.next();

                    System.out.println("year of writing");
                    int year = SCANNER.nextInt();

                    System.out.println("It's volume");
                    int volume = SCANNER.nextInt();

                    System.out.println("Is update ?");
                    boolean isUpdate = SCANNER.nextBoolean();

                    Article article = new Article(author1, author2, author3, author4, isUpdate, year,
                            volume, startPage, endPage, name);
                    library.addArticle(article);
                    break;
                }
                case 3: {
                    library.shoAllBooks();
                    break;
                }
                case 4: {
                    library.showAllArticles();
                    break;
                }
                case 10: {
                    return;
                }
            }
        }
    }

    public static void menu() {
        System.out.println(
                "1.Create new book\n" +
                        "2.Create new article\n" +
                        "3.Show all books\n" +
                        "4.Show all articles\n" +
                        "10.Exit");
    }
}

//        Заменить параметры на класс Reference, предложить рефакторинг

public class RefactorInheritance {

    public void processing(Book book) {

    }

    public void processing(Article article) {


//        do something with article

//        Заменить параметры на класс Reference, предложить рефакторинг
    }

}