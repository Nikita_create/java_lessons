package homeworks.inheritance.inheritance_films;

/*3) Создать класс Фильм со свойствами: Название, Режиссер, длительность
        (в минутах), количество актеров. Определить метод:
        «Стоимость», возвращающую примерную расчетную стоимость фильма
        (в тыс. $), рассчитываемую по формуле длительность*20 + количество
        актеров*30, но если режиссер = «Стивен Спилберг» или «Джеймс
        Кэмерон», то стоимость в два раза выше (по сравнению с вышеуказанной формулой). Определить также метод «Информация», который
        возвращает строку, содержащую информацию о фильме: Название,
        режиссера, длительность, количество актеров и стоимость.
        Создать также класс наследник Мультфильм, в котором переопределить метод «Стоимость» по формуле длительность*25 + количество
        актеров*10 (вне зависимости от режиссера).
        В главной программе (либо по нажатию на кнопку) создать 2 фильма с
        режиссерами: «Стивен Спилберг» и «Ежи Гофман», а также мультфильм и вывести информацию о них.*/

public class Film implements Filmable {
    private String name;
    private String director;
    private int duration;
    private int numberActor;

    public Film(String name, String director, int duration, int numberActor) {
        this.name = name;
        this.director = director;
        this.duration = duration;
        this.numberActor = numberActor;
    }

    public int getDuration() {
        return duration;
    }

    public int getNumberActor() {
        return numberActor;
    }

    @Override
    public void cost() {
        int cost = duration * 20 + numberActor * 30;
        if (director.equals("Steve Spilbarge") || director.equals("James Cameron")) {
            cost *= 2;
        }
        System.out.println(cost);
    }

    @Override
    public void info() {
        String info = "Name : " + this.name + ";" + " Director : " + this.director + ";" + " Duration : " +
                this.duration + ";" + " Number of actor " + this.numberActor + ";";
    }
}

class Cartoon extends Film implements Filmable {
    public Cartoon(String name, String director, int duration, int numberActor) {
        super(name, director, duration, numberActor);
    }

    @Override
    public void cost() {
        int cost = getDuration() * 25 + getNumberActor() * 10;
        System.out.println(cost);
    }

}

interface Filmable {
    void cost();

    void info();
}

class TestFilms {
    public static void main(String[] args) {
        Filmable joker = new Film("Joker", "Steve Spilbarge", 2, 11);
        Filmable moana = new Cartoon("Moana", "Raber Tringe", 2, 6);

        joker.cost();
        joker.info();

        moana.cost();
    }
}