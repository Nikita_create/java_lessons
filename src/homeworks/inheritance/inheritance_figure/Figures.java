package homeworks.inheritance.inheritance_figure;

public abstract class Figures implements Calculatable {
    private String name;
    private int sideLength;
    private int heightLength;

    public Figures(String name, int sideLength, int heightLength) {
        this.name = name;
        this.sideLength = sideLength;
        this.heightLength = heightLength;
    }

    public int getSideLength() {
        return sideLength;
    }

    public int getHeightLength() {
        return heightLength;
    }

    @Override
    public void print() {
        System.out.println(name);
    }
}

class Triangle extends Figures {
    public Triangle(String name, int sideLength, int heightLength) {
        super(name, sideLength, heightLength);
    }

    @Override
    public void calculateSquare() {

        double square = getSideLength() * getHeightLength() / 2;

        System.out.println(square);
    }
}

class Parallelogram extends Figures {
    public Parallelogram(String name, int sideLength, int heightLength) {
        super(name, sideLength, heightLength);
    }

    @Override
    public void calculateSquare() {
        int[] h = new int[getSideLength()];
        int a = getHeightLength();

        for (int i = 0; i < h.length; i++) {
            a += getHeightLength();
        }

        int square = getSideLength() * a;

        System.out.println(square);
    }
}

class Romb extends Figures {
    public Romb(String name, int sideLength, int heightLength) {
        super(name, sideLength, heightLength);
    }

    @Override
    public void calculateSquare() {
        int square = getHeightLength() * getSideLength();

        System.out.println(square);
    }
}

interface Calculatable {
    void calculateSquare();

    default void print() {
        System.out.println();
    }
}

class TestFigures {
    public static void main(String[] args) {
        Calculatable triangle = new Triangle("Triangle", 5, 7);
        Calculatable parallelogram = new Parallelogram("Parallelogram", 8, 4);
        Calculatable romb = new Romb("Romb", 7, 6);

        romb.print();
        romb.calculateSquare();

        triangle.calculateSquare();

        parallelogram.calculateSquare();
    }
}
