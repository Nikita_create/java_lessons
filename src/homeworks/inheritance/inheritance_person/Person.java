package homeworks.inheritance.inheritance_person;

public class Person implements Persons {
    private String firstName;
    private String lastName;
    private int age;
    private String gander;
    private int phoneNumber;

    public Person(String firstName, String lastName, int age, String gander, int phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gander = gander;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void printFirstName() {
        System.out.println(firstName);
    }

    @Override
    public void printLastName() {
        System.out.println(lastName);
    }

    @Override
    public void printAge() {
        System.out.println(age);
    }

    @Override
    public void printGender() {
        System.out.println(gander);
    }

    @Override
    public void printPhoneNumber() {
        System.out.println(phoneNumber);
    }
}

class Vlad extends Person {
    public Vlad(String firstName, String lastName, int age, String gander, int phoneNumber) {
        super(firstName, lastName, age, gander, phoneNumber);
    }
}

interface Persons {
    void printFirstName();

    void printLastName();

    void printAge();

    void printGender();

    void printPhoneNumber();
}

class TestPerson {
    public static void main(String[] args) {
        Persons vlad = new Vlad("Vlad", "Rogov", 21, "Male", 989698307);

        vlad.printFirstName();
        vlad.printLastName();
        vlad.printAge();
        vlad.printGender();
        vlad.printPhoneNumber();
    }
}
