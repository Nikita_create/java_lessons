package homeworks.inheritance.test_work;

public interface Ballable {

    int count = 0;

    void jump();

    default String getName() {

        return "John";
    }

}