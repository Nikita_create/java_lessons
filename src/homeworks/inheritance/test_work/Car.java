package homeworks.inheritance.test_work;

public class Car extends TrafficLight {

    private int countOfSpeeds;
    private int countOfDoors;
    private String model;

    public Car(String name, int countOfSpeeds, int countOfDoors, String model) {
        super(name);
        this.countOfSpeeds = countOfSpeeds;
        this.countOfDoors = countOfDoors;
        this.model = model;
    }

    public void drive() {
        System.out.println("Go...");
    }
}