package homeworks.inheritance.vehicle;

public interface Vehitable {
    void testOil(TypeOil typeOil);
    void someAction();
}
