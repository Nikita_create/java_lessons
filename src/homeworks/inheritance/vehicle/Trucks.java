package homeworks.inheritance.vehicle;

public abstract class Trucks extends Vehicle implements Vehitable{
    public Trucks(int numberOfWheels) {
        super(numberOfWheels);
        if (numberOfWheels <= 4){
            System.out.println("few wheels");
        }
    }

    @Override
    public void someAction() {
        System.out.println("I can drive anywhere");
    }
}
