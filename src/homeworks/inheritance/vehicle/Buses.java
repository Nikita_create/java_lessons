package homeworks.inheritance.vehicle;

public abstract class Buses extends Vehicle implements Vehitable{
    public Buses(int numberOfWheels) {
        super(numberOfWheels);
        if (numberOfWheels <= 4){
            System.out.println("few wheels");
        }
    }

    @Override
    public void someAction() {
        System.out.println("I can carry a lot of people");
    }
}
