package homeworks.inheritance.vehicle;

public enum TypeOil {
    GASOLINE, DIESEL, ELECTRICITY
}
