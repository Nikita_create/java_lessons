package homeworks.inheritance.vehicle;

public abstract class Car extends Vehicle {
    private TypeOil typeOil;

    public Car(int numberOfWheels, TypeOil typeOil) {
        super(numberOfWheels);
        this.typeOil = typeOil;
    }

    public TypeOil getTypeOil() {
        return typeOil;
    }
}

class Suv extends Car {
    public Suv(int numberOfWheels, TypeOil typeOil) {
        super(numberOfWheels, typeOil);
    }

    @Override
    public void someAction() {
        System.out.println("I can drive anywhere");
    }

    @Override
    public void testOil(TypeOil typeOil) {
        if(getTypeOil() == TypeOil.GASOLINE){
            System.out.println("Type of oil is not compatible with this type of car");
        }
    }
}

class RacingCar extends Car{
    public RacingCar(int numberOfWheels, TypeOil typeOil) {
        super(numberOfWheels, typeOil);
    }

    @Override
    public void someAction() {
        System.out.println("Я могу ездить очень быстро");
    }

    @Override
    public void testOil(TypeOil typeOil) {
        if (getTypeOil() == TypeOil.ELECTRICITY) {
            System.out.println("Type of oil is not compatible with this type of car");
        }
    }
}

class CompactCar extends Car{
    public CompactCar(int numberOfWheels, TypeOil typeOil) {
        super(numberOfWheels, typeOil);
    }

    @Override
    public void someAction() {
        System.out.println("I can drive on asphalt road");
    }

    @Override
    public void testOil(TypeOil typeOil) {
        if (getTypeOil() == TypeOil.DIESEL) {
            System.out.println("Type of oil is not compatible with this type of car");
        }
    }
}
