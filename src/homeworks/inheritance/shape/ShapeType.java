package homeworks.inheritance.shape;

public enum ShapeType {
    SQUARE, CIRCLE
}
