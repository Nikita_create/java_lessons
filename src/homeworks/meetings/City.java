package homeworks.meetings;

public enum City {
    KYIV,
    LVIV,
    KRYVOY_ROG,
    ODESSA
}
