package homeworks.meetings;

public class Meetings {

    private Person[] persons;

    public Meetings() {
        persons = new Person[10];//nulls
    }

    private void showSuitablePersons(Gender gender, int age) {
        for (Person p : persons) {
            if (p != null && p.getGender() != gender && p.getAge() == age) {
                p.view();
            }
        }
    }

    public void showOppositPersons(Gender gender) {

        for (Person p : persons) {
            if (p != null && p.getGender() != gender) {
                p.view();
            }
        }
    }

    public void searchByNameAndCity(String name, City city) {
        for (int i = 0; i < persons.length; i++) {
            if (persons[i] != null &&
                    persons[i].getCity() == city &&
                    persons[i].getFirstName().equals(name)) {
                persons[i].printPersons();
            }
        }
    }

    public void addPerson(Person person) {

        if (person.getAge() < 18) {
            System.out.println("Grow up");
            return;
        }

        for (int i = 0; i < persons.length; i++) {
            if (persons[i] == null) {
                persons[i] = person;
                break;
            }
        }

        showSuitablePersons(person.getGender(), person.getAge());

    }
}
