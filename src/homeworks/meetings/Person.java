package homeworks.meetings;

import java.sql.SQLOutput;

public class Person {
    private int age;
    private String firstName;
    private String lastName;
    private City city;
    private int children;
    private Gender gender;

    public Person(int newAge, String newFirsName, String newLastName,
                  City newCity, int newChilds, Gender newGender) {
        this.age = newAge;
        this.firstName = newFirsName;
        this.lastName = newLastName;
        this.city = newCity;
        this.children = newChilds;
        this.gender = newGender;
    }

    public int getAge() {
        return age;
    }

    public City getCity() {
        return city;
    }

    public String getFirstName() {
        return firstName;
    }

    public Gender getGender() {
        return gender;
    }

    public void view() {
        String info = "Age = " + this.age + "\tGender = " + this.gender + "\tFirst name = " + this.firstName;
//        String info = "Age = ".concat(String.valueOf(age));
        System.out.println(info);
    }

    public void printPersons() {
        String info = "Age = " + this.age + "\tGender = " + this.gender + "\tFirst name = " + this.firstName +
                "\tLast name = " + this.lastName + "\tCity = " + this.city + "\tChildren = " + this.children;
        System.out.println(info);
    }
}
