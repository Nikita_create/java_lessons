package homeworks.meetings;

public class TestMeetings {
    public static void main(String[] args) {
        Meetings meetings = new Meetings();

        Person person = new Person(19,"Oleg","Veselov",City.ODESSA,
                0,Gender.MALE);
        Person personM = new Person(24,"Maxim","Zagorod",City.KYIV,
                0,Gender.MALE);
        Person personF = new Person(23,"Anna","Klimenko",City.LVIV,
                0,Gender.FEMALE);
        Person personK = new Person(20,"Kate","Klimenko",City.LVIV,
                0,Gender.FEMALE);



        meetings.addPerson(person);
        meetings.addPerson(personM);
        meetings.addPerson(personF);
        meetings.addPerson(personK);

//        meetings.showSuitablePersons(person.getGender(), person.getAge());
        meetings.showOppositPersons(Gender.MALE);
        meetings.searchByNameAndCity("Maxim", City.KYIV);
    }
}
