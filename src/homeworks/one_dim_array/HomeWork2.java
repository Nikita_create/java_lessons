package homeworks.one_dim_array;

/*
Дан массив - 19  элементов целого типа. Найти сумму элементов, расположенных до первого отрицательного элемента.
Если отрицательных элементов нет, то выдать соответствующее сообщение.
*/

import java.util.Random;

public class HomeWork2 {
    public static void main(String[] args) {
        int[] array = new int[19];

        Random random = new Random();


        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10) - 2;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

        System.out.println();

        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] >= 0) {
                sum += array[i];
            } else {
                System.out.print(sum + "\t");
                System.out.println("There is negative value");
                break;
            }


        }
    }
}
