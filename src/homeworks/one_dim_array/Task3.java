package homeworks.one_dim_array;

/*Создать массив из 10 чисел, проинициализировать его элементы случайными числами от -5 до 20.
Если в массиве есть отрицательные элементы тогда вывести на консоль сообщение об этом.*/

import java.util.Random;

public class Task3 {
    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(25 + 1) - 5;
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                System.out.print("There is negative value " + array[i]);
                break;
            } else {
                continue;
            }
        }
    }
}
