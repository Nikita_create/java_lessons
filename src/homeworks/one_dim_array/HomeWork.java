package homeworks.one_dim_array;

/*
Ввести массив, состоящий из 20 элементов целого типа. Определить
каких элементов больше четных или нечетных по значению.
*/

import java.util.Random;

public class HomeWork {
    public static void main(String[] args) {
        int[] array = new int[17];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10) + 1;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

        int sumCH = 0;
        int sumNCH = 0;


        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 1) {
                sumNCH += array[i];
            }

            if (array[i] % 2 == 0) {
                sumCH += array[i];
            }
        }

        System.out.println("Сумма нечетных чисел = " + sumNCH);
        System.out.println("Сумма четных чисел = " + sumCH);

        System.out.println();

        if (sumCH >= sumNCH) {
            System.out.println("Суммма четных чисел больше чем нечетных: " + sumCH);
        } else {
            System.out.println("Суммма четных чисел меньше чем нечетных: " + sumNCH);
        }
    }
}
