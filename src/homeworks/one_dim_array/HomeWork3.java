package homeworks.one_dim_array;

/*
В массиве целых чисел с количеством элементов 19 определить максимальное число
и заменить им все четные по значению элементы.
*/

import java.util.Random;

public class HomeWork3 {
    public static void main(String[] args) {
        int[] array = new int[19];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10) + 1;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

        System.out.println();

        int max = array[0];

        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        System.out.print("Max number = " + max);


        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0){
                array[i] = max;
            }
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] +"\t");
        }
    }
}
