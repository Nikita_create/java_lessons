package homeworks.one_dim_array;

/*
Целочисленный массив, состоящий из 17 элементов. Найти сумму
и количество элементов, значение которых больше среднеарифметического положительных элементов.
*/

import java.util.Random;

public class HomeWork5 {
    public static void main(String[] args) {
        int[] array = new int[17];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(12) -5;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

        System.out.println();

        int y = 0;
        int sum =0;


        for (int i = 0; i < array.length; i++, y++) {
            if (array[i] >= 0) {
                sum += array[i];
            }
        }

        double avg = sum * 1.0 / y;
        int x = 0;
        int sum3 = 0;


        for (int i = 0; i < array.length; i++) {
            if (array[i] >= avg) {
                sum3 += array[i];
                x++;
            }
        }

        System.out.println("Среднеарефметическое положительных чисел = " + avg);
        System.out.println("Сумма = " + sum3 );
        System.out.println("Количество = " + x);
    }
}
