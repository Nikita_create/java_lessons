package homeworks.one_dim_array;

import java.util.Random;

/*
* Даны два массива действительных чисел по 12 элементов в каждом.
Заменить нулями те элементы первого массива, которые есть во втором.
* */
public class HomeWork6 {
    public static void main(String[] args) {
        int[] array = new int[12];
        int[] array2 = new int[12];

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(12) + 7;
            array2[i] = random.nextInt(12) + 7;
        }

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array2[j] == array[i]) {
                    array[i] = 0;
                }
            }
        }
    }
}

/*
 * 4 5 6 7 8 9
 * 4 2 1 0 7 1
 *
 *
 * */
