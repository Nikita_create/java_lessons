package homeworks.one_dim_array;

/*
Задан целочисленный массив. Определить остаток от деления суммы
элементов с четными индексами на сумму элементов с нечетными индексами.
*/

import java.util.Random;

public class HomeWork4 {
    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10) -4;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }

        System.out.println();

        int ch = 0;
        int nch = 0;

        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                ch += array[i];
            }

            if (i % 2 == 1) {
                nch += array[i];
            }
        }
        System.out.println("Сумма не четных чисел = " + nch);
        System.out.println("Сумма четных чисел = " + ch);

        System.out.println();

        int question = ch % nch;

        System.out.println("Остаток от деления суммы элементов с четными индексами на сумму элементов " +
                "с нечетными индексами = " + question);
    }
}
