package homeworks.string_array;

import java.util.Arrays;

public class Sentence {
    public static void main(String[] args) {
        String alf = "a b s d e f g";
        String sentence = "Bob Abram Fred Sam Georg Feel Dan";
        String[] str = sentence.split(" ");

        Arrays.sort(str);
        System.out.println(Arrays.toString(str));

    }
}