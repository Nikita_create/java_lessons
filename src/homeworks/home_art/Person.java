package homeworks.home_art;

public class Person {
    private String name;
    private String surname;

    public Person(String name, String soname) {
        this.name = name;
        this.surname = soname;
    }

    public String getName() {
        return name;
    }
}
