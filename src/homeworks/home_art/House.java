package homeworks.home_art;

public class House {
    private int number;
    private String houseOwner;//Person datatype

    public House(int number) {
        this.number = number;
        this.houseOwner = "";
    }

    public int getNumber() {
        return number;
    }

    public void setHouseOwner(String houseOwner) {
        this.houseOwner = houseOwner;
    }

    public void view() {
        String info = "This number of house = " + this.number + "\tThis house owner = " + this.houseOwner;
        System.out.println(info);
    }
}
