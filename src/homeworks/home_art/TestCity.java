package homeworks.home_art;

import java.util.Scanner;

public class TestCity {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        City city = new City();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            switch (key) {
                case 1: {
                    System.out.println("Enter number");

                    int number = SCANNER.nextInt();

                    House house = new House(number);

                    city.addHouse(house);
                    break;
                }

                case 2: {
                    System.out.println("Enter name");

                    String name = SCANNER.next();

                    System.out.println("Enter surname");

                    String surname = SCANNER.next();

                    Person person = new Person(name, surname);

                    city.addPerson(person);
                    break;
                }

                case 3: {
                    city.showCity();
                    break;
                }

                case 4: {
                    System.out.println("Enter name of man");

                    String name = SCANNER.next();

                    System.out.println("Enter number of house");

                    int number = SCANNER.nextInt();

                    city.giveHouse(name, number);
                    break;
                }

                case 10: {
                    return;
                }
            }
        }
    }

    public static void menu() {
        System.out.println(
                "1) Add house\n" +
                        "2) Add man\n" +
                        "3) Show all city\n" +
                        "4) Give a house\n" +
                        "10) Exit");
    }
}
