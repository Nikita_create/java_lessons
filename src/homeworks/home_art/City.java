package homeworks.home_art;

import java.util.Objects;

public class City {
    private House[] houses;
    private Person[] people;

    public City() {
        houses = new House[10];
        people = new Person[10];
    }

    public void addHouse(House house) {
        for (int i = 0; i < houses.length; i++) {
            if (houses[i] == null) {
                houses[i] = house;
                break;
            }
        }
    }

    private boolean existsPerson(String name) {
        for (Person p : people) {
            if (name.equals(p.getName())) {
                return true;
            }
        }
        return false;
    }

    public void giveHouse(String name, int numberHouse) {
        if (existsPerson(name)) {
            for (House s : houses) {
                if (Objects.nonNull(s) && s.getNumber() == numberHouse) {
                    s.setHouseOwner(name);
                    System.out.println();
                }
            }
        }
    }

    public void showCity() {
        for (House s : houses) {
            if (Objects.nonNull(s)) {
                s.view();
            }
        }
    }

    public void addPerson(Person person) {
        for (int i = 0; i < people.length; i++) {
            if (people[i] == null) {
                people[i] = person;
                break;
            }
        }
    }

    public void increase() {
        House[] temp = new House[houses.length * 2];
        if (houses[houses.length - 1] == null) {
            for (int i = 0; i < houses.length; i++) {
                temp[i] = houses[i];
            }
            houses = temp;
        }
    }
}
