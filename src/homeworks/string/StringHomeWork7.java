package homeworks.string;

public class StringHomeWork7 {
    public static void main(String[] args) {
        String sentence = "I go to home * now (at this moment)";

        System.out.println(sentence.indexOf("*"));


        char[] str = sentence.toCharArray();

        for (int s = 0; s < str.length; s++) {
            if (str[s] == '*') {
                System.out.println("First '*' is located on " + s + " place ");
                break;
            }
        }
    }
}
