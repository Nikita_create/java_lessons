package homeworks.string;

public class StringHomeWork6 {
    public static void main(String[] args) {
        String sentence = "I , ride . a . bicycle , ";

        char[] str = sentence.toCharArray();

        int numberOfDot = 0;
        int numberOfComma = 0;

        for (char s : str) {
            if (s == '.') {
                numberOfDot++;
            }
            if (s == ',') {
                numberOfComma++;
            }
        }

        System.out.println(numberOfComma);
        System.out.println(numberOfDot);
    }
}
