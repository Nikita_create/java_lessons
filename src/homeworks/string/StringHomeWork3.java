package homeworks.string;

public class StringHomeWork3 {
    public static void main(String[] args) {
        String sentence = "I work all day";

        String[] str = sentence.split(" ");

        for (String s : str) {
            if (s.length() == 3) {
                System.out.println(s);
            }
        }

    }
}
