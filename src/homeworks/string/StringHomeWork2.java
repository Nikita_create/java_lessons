package homeworks.string;

public class StringHomeWork2 {
    public static void main(String[] args) {
        String sentence = "Hello my friend";

        sentence = sentence.substring(1);

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < sentence.length(); i++) {

            char c = sentence.charAt(i);
            if (c == ' ') {
                builder.append(", ");
            } else {
                builder.append(c);
            }

        }

//        System.out.println(builder.toString());

        System.out.println(sentence);
    }
}
