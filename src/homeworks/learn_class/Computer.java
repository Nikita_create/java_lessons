package homeworks.learn_class;


public class Computer {
    private int serialNumber;
    private float price;
    private int quantityCPU;
    private int frequencyCPU;

    public Computer(int newSerialNumber, int newPrice, int newQuantityCPU, int newFrequencyCPU) {
        this.serialNumber = newSerialNumber;
        this.price = newPrice;
        this.quantityCPU = newQuantityCPU;
        this.frequencyCPU = newFrequencyCPU;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPrice(){
        return price;
    }

    public void view() {//this
        System.out.println("Serial number = " + this.serialNumber + " Price = " + this.price) ;
    }

}

class TestComputer {
    public static void main(String[] args) {
        Computer comp1 = new Computer(1234,900,6,4);
        Computer comp2 = new Computer(2341,200,2,1);
        Computer comp3 = new Computer(3412,800,6,3);
        Computer comp4 = new Computer(4123,500,4,2);
        Computer comp5 = new Computer(4125,500,4,2);

        Computer[] comps = new Computer[5];

        comps[0] = comp1;
        comps[1] = comp2;
        comps[2] = comp3;
        comps[3] = comp4;
        comps[4] = comp5;

        for (Computer c : comps) {
            float price = c.getPrice();

            float changedPrice = (float) (price + (price * 0.1));

            c.setPrice(changedPrice);

            c.view();
        }

    }
}