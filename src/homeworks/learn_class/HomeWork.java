package homeworks.learn_class;


/*
Создать конструктор.
Создать 5 объектов и изменить значения их полей price на 10%.
Создать метод вывода полей в консоль.
*/
public class HomeWork {
    private int sumMan;
    private int sumWoman;
    private int sumChild;

    public HomeWork() {
        this.sumMan = 1000;
        this.sumWoman = 500;
        this.sumChild = 100;
    }

    public void plus(int value) {
        if (value <= 0) {
            System.out.println("incorrect value");
        } else {
            int changedSumMan = sumMan / 100 * value;
            int woman = sumWoman / 100 * value;
            int child = sumChild / 100 * value;

            sumMan += changedSumMan;
            sumChild += child;
            sumWoman += woman;
        }
    }

    public void minus(int value) {
        if (value >= sumMan && value >= sumWoman && value >= sumChild || value <= 0) {
            System.out.println("incorrect value");
        } else {
            sumMan = 1000;
            sumWoman = 500;
            sumChild = 100;

            int man = sumMan / 100 * value;
            int woman = sumWoman / 100 * value;
            int child = sumChild / 100 * value;

            sumMan -= man;
            sumChild -= child;
            sumWoman -= woman;
        }
    }

    public int getSumMan() {
        return sumMan;

    }

    public int getSumWoman() {
        return sumWoman;
    }

    public int getSumChild() {
        return sumChild;
    }

}

class TestHomeWork {
    public static void main(String[] args) {
        HomeWork price = new HomeWork();

        int sumMan = price.getSumMan();
        int sumWoman = price.getSumWoman();
        int sumChild = price.getSumChild();

        System.out.println(sumMan + "$");
        System.out.println(sumWoman + "$");
        System.out.println(sumChild + "$");

        System.out.println();
        System.out.println("plus 10%");
        System.out.println();

        price.plus(10);

        int newSumMan = price.getSumMan();
        int newSumWoman = price.getSumWoman();
        int newSumChild = price.getSumChild();

        System.out.println(newSumMan + "$");
        System.out.println(newSumWoman + "$");
        System.out.println(newSumChild + "$");

        System.out.println();
        System.out.println("minus 10%");
        System.out.println();

        price.minus(10);

        int minusSumMan = price.getSumMan();
        int minusSumWoman = price.getSumWoman();
        int minusSumChild = price.getSumChild();

        System.out.println(minusSumMan);
        System.out.println(minusSumWoman);
        System.out.println(minusSumChild);
    }
}

class Man {
    private String type;
    private int sum;

    public Man(String type, int sum) {
        this.type = type;
        this.sum = sum;
    }

    public void plus(int value) {
        if (value <= 0) {
            System.out.println("incorrect value");
        } else {
            sum += sum / 100 * value;
        }
    }

    public void minus(int value) {
        if (value >= sum && value >= sum) {
            System.out.println("incorrect value");
        } else {
            int changedSum = sum / 100 * value;
        }
    }
}

class TestMan {
    public static void main(String[] args) {
        Man man = new Man("Man", 0);

        man.plus(50);

        Man woman = new Man("Woman", 0);

        woman.plus(40);

        Man child = new Man("Child", 0);

        child.plus(45);
    }
}

