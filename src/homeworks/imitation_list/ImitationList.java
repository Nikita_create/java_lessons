package homeworks.imitation_list;


import java.util.Random;

public class ImitationList {//homeworks.imitation_list.ImitationList
    private int[] list;

    public ImitationList(int size) {//1000000
        list = new int[size]; //0 0
    }

    //TODO Only for test puproses
    public int[] getList() {
        return list;
    }

    //TODO Only for test puproses
    public void setList(int[] list) {
        this.list = list;
    }

    public void addElementToList(int value) {

        resize();

        for (int i = 0; i < list.length; i++) {
            if (list[i] == 0) {
                list[i] = value;
                break;
            }
        }
    }

    public void increaseList(int newSize) {

        int[] temp = new int[list.length + newSize];

        for (int i = 0; i < list.length; i++) {
            temp[i] = list[i];
        }

        list = temp;
    }

    public void decreaseList(int newSize) {

        if (newSize >= list.length) {
            System.out.println("Incorrect incoming param");
            return;
        }

        int[] temp = new int[list.length - newSize];

        for (int i = 0; i < temp.length; i++) {
            temp[i] = list[i];
        }

        list = temp;
    }

    private void resize() {
        if (list[list.length - 1] != 0) {

            int[] temp = new int[list.length * 2];

            for (int i = 0; i < list.length; i++) {
                temp[i] = list[i];
            }

            list = temp;
        }
    }

    public void showAllList() {
        for (int s : list) {
            System.out.print(s + "\t");
        }
    }

    public void deleteElementByIndex(int index) {
        int[] temp = new int[list.length - 1];

        for (int i = 0; i < index; ++i) {
            temp[i] = list[i];
        }

        for (int j = index; j < list.length - 1; ++j) {
            temp[j] = list[j + 1];
        }

        list = temp;
    }

    public void changeElementByIndex(int index, int newNumber) {
        if (index > list.length - 1) {
            System.out.println("Incorrect index");
            return;
        }
        //5 6 0 1 0 0 + 3 -> 5 6 3 1 0 0
        if (newNumber == 0) {//5 6 9 1 0 0 + 3 -> 5 6 9 1 3 0
            System.out.println("Incorrect value");
            return;
        }

        list[index] = newNumber;
    }

    public void showListInBack() {
        for (int i = list.length - 1; i >= 0; i--) {
            System.out.println(list[i]);
        }
    }

    // 1 2 3 0  + 7 8 9 -> 1 2 3 7 8 9 0
    public void addArray(int[] array) {

        int countZeros = 0;

        for (int e : list) {
            if (e == 0) {
                countZeros++;
            }
        }

        int positionZero = list.length - countZeros;

        if (countZeros < array.length) {
            increaseList(array.length - countZeros + 1);//3 - 1 = 2
        }
//1 2 3 0 0 0 0
        for (int i = positionZero, j = 0; j < array.length; i++, j++) {
            list[i] = array[j];
        }

    }

    public void bubbleSort() {
        for (int i = list.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {

                if (list[j] > list[j + 1]) {
                    int tmp = list[j];
                    list[j] = list[j + 1];
                    list[j + 1] = tmp;
                }
            }
        }
    }

    //1 2 7 3 4 7 -> 1 2 7 3 4
    public void deleteDuplicates() {
        for (int i = 0; i < list.length; i++) {//i == 2; j == 5
            for (int j = i + 1; j < list.length; j++) {//j == 5
                if (list[i] == list[j] && list[j] != 0) {
                    deleteElementByIndex(j);
                    --j;
                }
            }
        }
    }

    // 1 2 3 5 7 5 0
    public void searchFirstPosition(int value) {//12

        boolean isValue = false;

        for (int i = 0; i < list.length; i++) {
            if (list[i] == value) {
                System.out.println("Index of first " + value + " is " + i);
                isValue = true;
                break;
            }
        }

        if (!isValue) {
            System.out.println("There isn't the " + value);
        }
    }

    // 1 2 5 6 8 -> 1 2 8 6 5
    /*
     * temp = 8;
     * j = 2; list[j] = 5
     * list[4] = 8;
     * list[4] = list[2]
     * */
    public void shuffling() {
        Random random = new Random();

        for (int i = list.length - 1; i > 1; i--) {
            int j = random.nextInt(i);//0 - 10

            int temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}
