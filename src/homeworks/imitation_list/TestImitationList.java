package homeworks.imitation_list;

public class TestImitationList {
    public static void main(String[] args) {
        ImitationList list = new ImitationList(11);

        list.addElementToList(1);
        list.addElementToList(2);
        list.addElementToList(3);
        list.addElementToList(4);
        list.addElementToList(3);
        list.addElementToList(1);
        list.addElementToList(6);

//        list.deleteElementByIndex(4);

//        list.changeElementByIndex(3,7);

        list.showAllList();

        int[] array = {7, 8, 9};

//        list.addArray(array);

//        list.deleteDuplicates();
        System.out.println();

//        list.searchFirstPosition(30);

        list.shuffling();

        list.showAllList();

        System.out.println();

//        list.showListInBack();
    }
}
